<nav class="navbar navbar-expand-lg navbar-dark bg-warning" id="navbarstyle">
 <a class="navbar-brand" href="#">HAPPIFY</a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
 </button>
 <div class="collapse navbar-collapse" id="navbarColor01">
   <ul class="navbar-nav mr-auto">
    <?php 
      session_start();
      if (!isset($_SESSION['user'])) {
        ?>
                <li class="nav-item">
                  <a class="nav-link text-white" href="../views/landing_page.php">Home</a>
                </li>
      <?php
      }
      ?>


      <li class="nav-item">
        <a class="nav-link text-white" href="../views/playlists_form.php">Songs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="../views/featured_artists.php">Featured Artists</a>
      </li>

      <?php
      if (isset($_SESSION['user'])) {
        ?>
        <li class="nav-item">
        <a class="nav-link text-white" href="../views/add_song_form.php">Add Song</a>
        </li>

        <li class="nav-item">
          <a class="nav-link text-white" href="../views/edit_user_profile.php">Edit User Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="../controllers/process_logout.php">logout</a>
        </li>
      <?php
      }else{
        ?>
        <li class="nav-item">
        <a class="nav-link text-white" href="../views/login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="../views/register.php">Register</a>
      </li>
      <?php
      }
      ?>


    </ul><!-- 
   <form class="form-inline my-2 my-lg-0">
     <input class="form-control mr-sm-2" type="text" placeholder="Search">
     <button class="btn btn-light my-2 my-sm-0" type="submit">Search</button>
   </form> -->
 </div>
</nav>