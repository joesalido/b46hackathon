<html>
<head>
    <meta charset="UTF-8">
    <title>HACKATHON</title>
       <!-- Google Font -->
       <link href="https://fonts.googleapis.com/css?family=PT+Mono%7CRajdhani%7CSource+Code+Pro&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Rajdhani%7CSource+Code+Pro&display=swap" rel="stylesheet" >
   <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Anton%7CDosis:800&display=swap" rel="stylesheet">
   <!-- FONT AWESOME -->
   <script src="https://kit.fontawesome.com/47d9517eee.js" crossorigin="anonymous"></script>
   <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
   <!-- BOOTSTRAP CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://bootswatch.com/4/minty/bootstrap.css">
   <!-- AOS CSS -->
   <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
   <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
   <!-- FONT AWESOME ICON LIBRARY -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!-- CSS -->
   <link rel="stylesheet" href="../assets/styles/style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body class="bg-warning">
   <?php require "navbar.php";
       get_content();
        require "footer.php";
    ?>
</body>
</html>