<?php
   require("../templates/template.php");
   
   function get_content() {
   ?>
    <div class="container h-100 text-center my-5 py-5">
                <div>
                    <div class="header">
                        <h1 class="title">WELCOME TO HAPPIFY! <br> HOW ARE YOU?</h1>
                        <h3> LISTEN TO SONGS BASED ON HOW YOU ARE FEELING </h3>
                        <form action="login.php">
                        <button type="submit" class="btn btn-outline-light">Login</button>
                        </form>
                        <form action="register.php">
                        <button type="submit" class="btn btn-outline-light">Register</button>
                        </form>
                  

                    </div>  
                </div>
    </div>
    
   
    <div class="jumbotron bg-info mx-5 my-5 py-5" id="jumbo">
        <h1 class="">FIND YOUR VIBE</h1>
            <p class="lead text-white">Tell us your mood or how you're feeling and we will give you a perfectly curated playlist, just for YOU!</p>
                <hr class="my-4">
                <p class="text-white">Discover, stream, and share a constantly expanding mix of music from emerging and major artists around the world.</p>
            <p class="lead">
                <a class="btn btn-outline-light " href="#" role="button">Sign Up</a>
            </p>
    </div>
   <?php
   }
?>