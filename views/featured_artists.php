<?php
   require("../templates/template.php");
   
   function get_content() {
    require '../controllers/connection.php';

   ?>

<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">


    <div class="carousel-item active" >
      <img src="../assets/images/music2.jpg" alt="Los Angeles" width="1100" height="500">
      <div class="carousel-caption">
        <h1>Harry Styles</h1>
        <p>We had such a great time in LA!</p>
        <button type="button" class="btn btn-warning">View Artist</button>
      </div>   
    </div>

    <?php 
      $query = "SELECT * FROM users";
      $artist = mysqli_query($conn,$query);

      foreach ($artist as $indiv_artist){
        ?>
          <div class="carousel-item">
            <img src="<?php echo $indiv_artist['image'];?>" width="1100" height="500">
            <div class="carousel-caption">
              <h1><?php echo $indiv_artist['username'] ?></h1>
              <p><?php echo $indiv_artist['about_me'] ?></p>
              <!-- <button type="button" class="btn btn-warning">View Artist</button> -->
            </div>   
          </div>

        <?php

      }

     ?>
    <div class="carousel-item">
      <img src="../assets/images/music3.jpg" alt="Chicago" width="1100" height="500">
      <div class="carousel-caption">
        <h1>The Beatles</h1>
        <p>Thank you, Chicago!</p>
        <button type="button" class="btn btn-warning">View Artist</button>
      </div>   
    </div>

    <div class="carousel-item">
      <img src="../assets/images/music4.jpg" alt="New York" width="1100" height="500">
      <div class="carousel-caption">
        <h1>Ariana Grande</h1>
        <p>We love the Big Apple!</p>
        <button type="button" class="btn btn-warning">View Artist</button>
      </div>   
    </div>

  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<?php
   }
?>