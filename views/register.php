<?php 
	require '../templates/template.php';
	function get_content(){
?>
	<h1 class="text-center py-5">Register</h1>
	<div class="col-lg-6 offset-lg-3">
		<form>
			<!-- <div class="form-group">
			first name
			<label for="FName">First Name</label>
			<input type="text" name="FName" class="form-control" id="">
			</div>
			<div class="form-group">
			last name
				<label for="LName">Last Name</label>
				<input type="text" name="LName" class="form-control" id="">
			</div> -->
			<div class="form-group">
				<!-- email -->
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" id="email">
				<span style="color:red; font-size:8pt"></span>
			</div>
			<div class="form-group">
				<!-- online alias/username -->
				<label for="username">Username</label>
				<input type="text" name="username" class="form-control" id="username">
				<span style="color:red; font-size:8pt"></span>
			</div>
			<div class="form-group">
				<!-- password -->
				<label for="password">Password</label>
				<input type="password" name="password" class="form-control" id="password">
				<span style="color:red; font-size:8pt"></span>
			</div>
			<div class="form-group">
				<!-- confirm password -->
				<label for="confirm">Confirm Password</label>
				<input type="password" name="confirm" class="form-control" id="confirm">
				<span style="color:red; font-size:8pt"</span>
			</div>
			<div class="text-center">
				<label>Remember me: <input type="checkbox" name="remember"></label>
			</div>
		</form>
		<div class="text-center">						
			<button type="submit" class="btn btn-info text-center" id="registerUser">Create Account</button>
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/register.js"></script>
<?php
}
?>