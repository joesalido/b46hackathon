<?php 
	require '../templates/template.php';
	function get_content(){
		require "../controllers/connection.php";

?>
	<h1 class="text-center py-5">EDIT USER PROFILE</h1>
		<div class="container">
			<div class="col-lg-6 offset-lg-3">
				<form action="../controllers/process_edit_profile.php" method="POST" enctype="multipart/form-data">
					<?php
						$user_id = $_SESSION['user']['id'];
						$user_query = "SELECT * FROM users WHERE id = $user_id";
						$user = mysqli_fetch_assoc(mysqli_query($conn,$user_query));
					 ?>
					<div class="form-group">
						<label for="username">User Name:</label>
						<input type="text" name="username" class="form-control" value="<?php echo $user['username'] ?>">
					</div>
					<div class="form-group">
						<label for="firstName">First Name:</label>
						<input type="text" name="firstName" class="form-control"  value="<?php echo $user['firstName'] ?>">
					</div>
					<div class="form-group">
						<label for="lastName">Last Name:</label>
						<input type="text" name="lastName" class="form-control"  value="<?php echo $user['lastName'] ?>">
					</div>
					<div class="form-group">
						<label for="about_me">Say something about yourself:</label>
						<textarea name="about_me" class="form-control" > <?php echo $user['about_me'] ?></textarea>
					</div>
					<div class="form-group">
						<label for="image">Profile Image:</label>
						<img src="<?php echo $user['image']?>" height="50px" width="50px">
						<input type="file" name="image" class="form-control">
					</div>
					<input type="hidden" value="<?php echo $user['id'] ?>" name="id">
					<button type="submit" class="btn btn-success">Edit Profile</button>
				</form>
			</div>
		</div>
<?php

}
?>