<?php 
	require '../templates/template.php';
	function get_content(){
		require '../controllers/connection.php';


	?>
		<h1 class="text-center py-5">ADD SONG FORM</h1>
		<div class="container">
			<div class="col-lg-6 offset-lg-3">
				<form action="../controllers/process_add_song.php" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label for="year">Year:</label>
						<input type="number" name="year" class="form-control">
					</div>
					<div class="form-group">
						<label for="length">Length:</label>
						<input type="number" name="length" class="form-control">
					</div>					
					<div class="form-group">
						<label for="genre_id">Song Genre:</label>
						<select name="genre_id" class="form-control">
							<?php 

								$genre_query = "SELECT * FROM genres";
								$genres = mysqli_query($conn,$genre_query);
								foreach ($genres as $indiv_genre){
									?>
										<option value="<?php echo $indiv_genre['id'] ?>"><?php echo $indiv_genre['name'] ?></option>
									<?php
								}

							 ?>
						</select>
					</div>					
					<div class="form-group">
						<label for="img_file">Song Image:</label>
						<input type="file" name="img_file" class="form-control">
					</div>			
					<div class="form-group">
						<label for="song_file">Song File:</label>
						<input type="file" name="song_file" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['id']?>">
					<button type="submit" class="btn btn-success">Add Song</button>
				</form>
			</div>
		</div>
	<?php
	}
 ?>