<?php 
	require '../templates/template.php';
	function get_content(){
		require '../controllers/connection.php';

	?>
		<h1 class="text-center py-5">EDIT SONG FORM</h1>
		<div class="container">
			<div class="col-lg-6 offset-lg-3">
				<form action="../controllers/process_edit_song.php" method="POST" enctype="multipart/form-data">
					<?php
						$song_id = $_GET['id'];
						$song_query = "SELECT * FROM songs WHERE id = $song_id";
						$song = mysqli_fetch_assoc(mysqli_query($conn,$song_query));
					 ?>
					<div class="form-group">
						<label for="name">Song Name:</label>
						<input type="text" name="name" class="form-control" value="<?php echo $song['name'] ?>">
					</div>
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" class="form-control" ><?php echo $song['description'] ?></textarea>
					</div>

					<div class="form-group">
						<label for="year">Year:</label>
						<input type="number" name="year" class="form-control" value="<?php echo $song['year'] ?>">
					</div>
					<div class="form-group">
						<label for="length">Length:</label>
						<input type="number" name="length" class="form-control"  value="<?php echo $song['length'] ?>">
					</div>		
					<div class="form-group">
						<label for="genre_id">Song Genre:</label>
						<select name="genre_id" class="form-control">
							<?php 

								$genre_query = "SELECT * FROM genres";
								$genres = mysqli_query($conn,$genre_query);
								foreach ($genres as $indiv_genre){
									?>
										<option value="<?php echo $indiv_genre['id'] ?>"
											<?php echo $indiv_genre['id']==$song['genre_id'] ? "selected" : "" ?>
										><?php echo $indiv_genre['name'] ?></option>
									<?php
								}

							 ?>
						</select>
					</div>						
					<div class="form-group">
						<label for="img_file">Song Image:</label>
						<img src="<?php echo $song['img_file']?>" height="50px" width="50px">
						<input type="file" name="img_file" class="form-control">
					</div>			
					<div class="form-group">
						<label for="song_file">Song File:</label>
							<audio controls>
								<source src="<?php echo $song['song_file']?>" type="audio/mpeg">
							</audio>
						<input type="file" name="song_file" class="form-control">
					</div>
					<input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['id']?>">
					<input type="hidden" value="<?php echo $song['id'] ?>" name="id">
					<button type="submit" class="btn btn-success">Edit Song</button>
				</form>
			</div>
		</div>
	<?php
	}
 ?>