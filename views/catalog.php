<?php 
	require '../templates/template.php';
	function get_content(){
		require "../controllers/connection.php";

?>
	<h1 class="text-center py-5">CATALOG PAGE</h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-2">
				<h3>Genres</h3>
				<ul class="list-group border">
					<li class="list-group-item">
						<a href="catalog.php">All</a>
					</li>
					<?php 
						$genres_query = "SELECT * FROM genres";
						$genres = mysqli_query($conn,$genres_query);
						foreach ($genres as $indiv_genre){
							?>
								<li class="list-group-item">
									<a href="catalog.php?category_id=<?php echo $indiv_genre['id'] ?>"><?php echo $indiv_genre['name'] ?></a>
								</li>
							<?php
						}
					 ?>
				</ul>
				<!-- sorting -->
				<h3>Sort By</h3>
				<ul class="list-group border">
					<li class="list-group-item">
						<a href="../controllers/process_sort.php?sort=asc">Price (Lowest to Highest)</a>
					</li>
					<li class="list-group-item">
						<a href="../controllers/process_sort.php?sort=desc">Price (Highest to Lowest)</a>
					</li>
				</ul>
			</div>
			
				<!-- item list -->
			<div class="col-lg-10">
				<div class="row">
					<?php 
die();
			// steps for retrieving items
			//  1. Create query
			//	2. use mysqli_query
			//  3. if array USE foreach
			//  4. if Object use fetch_assoc
			$items_query = "SELECT * FROM items";
			if(isset($_GET['category_id'])){
				$catID = $_GET['category_id'];
				$items_query .= " WHERE category_id = $catID";
			}
			//sort
			if(isset($_SESSION['sort'])){
				$items_query .= $_SESSION['sort'];
			}
			// $items_query = "SELECT a.*, b.name as catName FROM items a LEFT JOIN categories b ON b.id = a.category_id"
			// <p class="card-text"><?php echo $indiv_item['catName']></p>
			$items = mysqli_query($conn,$items_query);
			foreach ($items as $indiv_item) {
				?>
					<div class="col-lg-4 py-2">
						<div class="card h-100">
							<img class="card-img-top" src="<?php echo $indiv_item['image'] ?>" alt="image">
							<div class="card-body">
								<h4 class="card-title"><?php echo $indiv_item['name'] ?></h4>
								<p class="card-text">Php <?php echo $indiv_item['price'] ?>.00</p>
								<p class="card-text"><?php echo $indiv_item['description'] ?></p>
								
								<?php
								$catID = $indiv_item['category_id'];
								$category_query = "SELECT * FROM categories WHERE id = $catID";
								$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
								
								// foreach($category as $key => $value){
								?>
									<p class="card-text"><?php echo $category['name'] ?></p>
							</div>

							<?php 
								if(isset($_SESSION['user']) && $_SESSION['user']['role']==1)
								{
									?>
										<div class="card-footer">
											<a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-secondary">Edit Item</a>
											<a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-danger">Delete Item</a>
										</div>
									<?php

								}else{
									?>
										<div class="card-footer">
											<!-- <form action="../controllers/process_update_cart.php" method="POST">
												<input type="number" class="form-control" value="1" name="quantity">
												<input type="hidden" name="id" value="<?php //echo $indiv_item['id'] ?>">
												<button type="submit" class="btn btn-primary">Add To Cart</button>
											</form> -->
											<input type="number" class="form-control" value="1" name="">
											<button type="button" class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id'] ?>">Add To Cart</button>
										</div>
									<?php
								}




							 ?>
							
						</div>
					</div>
				<?php
			}
			?>
				</div>
			</div>
			
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/addtocart.js"></script>
<?php

}
?>