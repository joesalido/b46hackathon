<?php
   require("../templates/template.php");
   function get_content() {
    require '../controllers/connection.php';
   ?>
    <div class="container text-center my-5">
        <h1>How are you feeling today?</h1>
        <div class="form-group">
        <label for="genre_id">Select a genre:</label>
        <select name="genre_id" class="form-control">
              <?php 
                $genre_query = "SELECT * FROM genres";
                $genres = mysqli_query($conn,$genre_query);
                foreach ($genres as $indiv_genre){
                  ?>
                    <option value="<?php echo $indiv_genre['id'] ?>"><?php echo $indiv_genre['name'] ?></option>
                  <?php
                }
               ?>
          </select>
        </div>
    </div>
    <section class="welcome-area text-center">

    <div class="container">
    <div class="row">
              <!-- item list -->
      <div class="col-lg-10">
        <div class="row">
          <?php 
      // steps for retrieving items
      //  1. Create query
      //  2. use mysqli_query
      //  3. if array USE foreach
      //  4. if Object use fetch_assoc
      $songs_query = "SELECT * FROM songs";
      if(isset($_GET['genre_id'])){
        $genreID = $_GET['genre_id'];
        $songs_query .= " WHERE genre_id = $genreID";
      }
      //sort
            // $items_query = "SELECT a.*, b.name as catName FROM items a LEFT JOIN categories b ON b.id = a.category_id"
      // <p class="card-text"><?php echo $indiv_item['catName']></p>
      $songs = mysqli_query($conn,$songs_query);
      foreach ($songs as $indiv_song) {
        ?>
          <div class="col-lg-4 py-2">
            <div class="card">
              <img class="card-img-top" src="<?php echo $indiv_song['img_file'] ?>" alt="image">
              <div class="card-body">
                <h4 class="card-title"><?php echo $indiv_song['name'] ?></h4>
          
                <audio controls id="player">
                  <source src="<?php echo $indiv_song['song_file']?>" type="audio/mpeg">
                </audio>
                <p class="card-text">Year: <?php echo $indiv_song['year'] ?></p>
                <p class="card-text"><?php echo $indiv_song['description'] ?></p>
                
                <?php
                $genreID = $indiv_song['genre_id'];
                $genre_query = "SELECT * FROM categories WHERE id = $genreID";
                $genre = mysqli_fetch_assoc(mysqli_query($conn, $genre_query));
                
                // foreach($category as $key => $value){
                ?>
                  <p class="card-text"><?php echo $genre['name'] ?></p>
              </div>
              <?php 

                if(isset($_SESSION['user']) AND $indiv_song['user_id']==$_SESSION['user']['id'])
                {
                  ?>
                    <div class="card-footer">
                      <a href="edit_song_form.php?id=<?php echo $indiv_song['id']?>" class="btn btn-secondary">Edit Item</a>
                      <a href="../controllers/process_delete_song.php?id=<?php echo $indiv_song['id']?>" class="btn btn-danger">Delete Item</a>
                    </div>
                  <?php

                }

              ?>
              
            </div>
          </div>
        <?php
      }
      ?>
        </div>
      </div>
      
    </div>
  </div>




  <!-- ***** Featured Guests Area Start ***** -->
  <section class="featured-guests-area my-5 py-5" >
    <div class="container">
      <div class="row">
        <!-- Section Heading -->
        <div class="col-12">
          <div class="section-heading text-center">
            <h2>Featured Guests</h2>
            <div class="line"></div>
          </div>
        </div>
      </div>

      <div class="row justify-content-around">
        <!-- Single Featured Guest -->
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="single-featured-guest mb-80">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTq2NW0lua55O038UmRfyZOv5AMWw7VPpWW9g_GoxPo85gEKpbK" alt="">
            <div class="guest-info">
              <h5>Alfred Day</h5>
              <span>ARTIST</span>
            </div>
          </div>
        </div>

        <!-- Single Featured artist -->
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="single-featured-guest mb-80">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQsUfX30YP9gXUt3Erzjr6WEDkX8UKE-ZopxDZfJshSeaEfeR5Y" alt="">
            <div class="guest-info">
              <h5>Jayden White</h5>
              <span>ARTIST</span>
            </div>
          </div>
        </div>

        <!-- Single Featured artist -->
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="single-featured-guest mb-80">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4x00ozHMwZ3cCFXp5DP2k144wyLrUzciOVpvt7wg4iPlrUc8y" alt="">
            <div class="guest-info">
              <h5>Vincent Reid</h5>
              <span>ARTIST</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ***** Featured Guests Area End ***** -->       
        </div>
       </div>
    </div>
  </section>
   <?php
   }
?>