// add an eventlistener to the button
// capture the details
// create new formdata
// append the data
// use fetch to access our process_authenticate.php

loginUser.addEventListener("click", ()=>{
	let email = document.querySelector("#email").value;
	let password = document.querySelector("#password").value;

	let data = new FormData;
	data.append("email", email);
	data.append("password", password);

	fetch("../../controllers/process_authenticate.php",{
		method: "POST",
		body: data
	}).then(response=>{
		return response.text();
	}).then(data_from_fetch=>{
		console.log(data_from_fetch);
		if(data_from_fetch=="login_failed"){
			document.querySelector("#email").nextElementSibling.innerHTML = "Please provide correct credentials";
		}else{
			if(data_from_fetch=="profile"){
				window.location.replace("../../views/edit_user_profile.php");
			}else{
				window.location.replace("../../views/featured_artists.php");
			}
		}
	})

})