function validate_reg_form(){
	errors = 0;
	let username = document.querySelector("#username").value;
	let email = document.querySelector("#email").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;

	// no empty field
	// username must be atleast 8 characters long, max 24
	// password must be atleast 8 characters long, max 24
	// confirm = password
	
	if(email==""){
		document.querySelector("#email").nextElementSibling.innerHTML="**Please provide a valid Email";
		errors++;
	}else{
		document.querySelector("#email").nextElementSibling.innerHTML="";
	}

	if (username.length<8 || username.length >24) {		
		document.querySelector("#username").nextElementSibling.innerHTML="**Username must be between 8-24 characters long";
		errors++;
	}else{
		document.querySelector("#username").nextElementSibling.innerHTML="";
	}
	if (password.length<8 || password.length >24) {		
		document.querySelector("#password").nextElementSibling.innerHTML="**Password must be between 8-24 characters long";
		errors++;
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML="";
	}

	if (password!=confirm) {
		document.querySelector("#confirm").nextElementSibling.innerHTML="**Please input the same password";
		errors++;
	}else{		
		document.querySelector("#confirm").nextElementSibling.innerHTML="";
	}

	if (errors> 0){
		return false;
	}else{
		return true;
	}
}


document.querySelector('#registerUser').addEventListener("click", ()=>{

	if(validate_reg_form()){
		// get the values	
		let username = document.querySelector("#username").value;
		let email = document.querySelector("#email").value;
		let password = document.querySelector("#password").value;

		let data = new FormData;
		data.append("email", email);
		data.append("username", username);
		data.append("password", password);

		fetch("../../controllers/process_register_user.php",{
			method: "POST",
			body: data
		}).then(response=>{
			return response.text();
		}).then(data_from_fetch=>{
			console.log(data_from_fetch);
			if(data_from_fetch=="user_exists"){
				document.querySelector("#email").nextElementSibling.innerHTML="User already exists";
			}else{
				window.location.replace("../../views/login.php");
			}
		})
	}
})