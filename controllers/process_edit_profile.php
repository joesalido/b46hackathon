<?php 
	require 'connection.php';
	function validate_form(){
		$errors = 0;
		$file_types = ["jpg","jpeg","png","gif", "bmp", "svg"];
		$file_ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
		if ($_POST['username']=="" || !isset($_POST['username'])) {
			$errors++;
		}
		if ($_POST['firstName']=="" || !isset($_POST['firstName'])) {
			$errors++;
		}
		if ($_POST['lastName']=="" || !isset($_POST['lastName'])) {
			$errors++;
		}
		if ($_POST['about_me']=="" || !isset($_POST['about_me'])) {
			$errors++;
		}
		if ($_FILES['image']['name'] != "") {
			if (!in_array($file_ext, $file_types)) {
				$errors++;
			}
		}
		if ($errors>0){
			return false;
		}else{
			return true;
		}
	}
	if (validate_form()) {
		// Process of saving an item
		// 1. capture all data from form through $_POST or $FILES for image
		// 2. move uploaded image file to the assets/images directory
		// 3. create the query
		// 4. use mysqli_query
		// 5. go back to catalog if successful
		// 6. if unsuccessful go back to add_item_form
		$id = $_POST['id'];
		$username = $_POST['username'];
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$about_me = $_POST['about_me'];

		$user_query = "SELECT image FROM users WHERE id = $id";
		$image_result = mysqli_fetch_assoc(mysqli_query($conn,$user_query));
		// var_dump($image_result);
		// die();

		$image = "";
		// check if the user uploaded a new image
		// if not, save the old value of image to $image
		// if yes, do the process of moving files
		if ($_FILES['image']['name'] == "") {
			$image = $image_result['image'];
		}else{
			$destination = "../assets/images/";
			$file_name = $_FILES['image']['name'];
			move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);
			$image = $destination.$file_name;
		}
		$update_users_query = "UPDATE users SET username ='$username', firstName = '$firstName', lastName='$lastName', about_me = '$about_me', image = '$image', isNewUser = false WHERE id = $id";
		$result = mysqli_query($conn,$update_users_query);
		header("Location: ../views/catalog.php");
	}else{
		header("Location: " .$_SERVER['HTTP_REFERER']);
	}
 ?>