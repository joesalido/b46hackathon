<?php 
	require 'connection.php';
	function validate_form(){
		$errors = 0;
		// validation logic
		// We'll check if each of the fields in the form has a value. if not, it will increase the $errors total and if the $errors total > 0, it will return false.
		// we'l lcehck if the file extension of the image is within the acceptable file extensions. if not, it will increase the $errors total and if the $errors total > 0, it will return false.
		$img_file_types = ["jpg","jpeg","png","gif", "bmp", "svg"];
		$img_file_ext = strtolower(pathinfo($_FILES['img_file']['name'], PATHINFO_EXTENSION));		
		if (!in_array($img_file_ext, $img_file_types)) {
			$errors++;
		}
		$song_file_types = ["mp3","wav"];
		$song_file_ext = strtolower(pathinfo($_FILES['song_file']['name'], PATHINFO_EXTENSION));		
		if (!in_array($song_file_ext, $song_file_types)) {
			$errors++;
		}


		if ($_POST['name']=="" || !isset($_POST['name'])) {
			$errors++;
		}
		if ($_POST['description']=="" || !isset($_POST['description'])) {
			$errors++;
		}
		if ($_POST['year']<=0 || !isset($_POST['year'])) {
			$errors++;
		}
		if ($_POST['length']<=0 || !isset($_POST['length'])) {
			$errors++;
		}
		if ($_POST['genre_id']=="" || !isset($_POST['genre_id'])) {
			$errors++;
		}
		if ($_FILES['img_file']=="" || !isset($_FILES['img_file'])) {
			$errors++;
		}
		if ($_FILES['song_file']=="" || !isset($_FILES['song_file'])) {
			$errors++;
		}
		if ($errors>0){
			return false;
		}else{
			return true;
		}
	}
	if (validate_form()) {
		// Process of saving an item
		// 1. capture all data from form through $_POST or $FILES for image
		// 2. move uploaded image file to the assets/images directory
		// 3. create the query
		// 4. use mysqli_query
		// 5. go back to catalog if successful
		// 6. if unsuccessful go back to add_item_form		
		$id = $_POST['id'];
		$name = $_POST['name'];
		$description = $_POST['description'];
		$year = $_POST['year'];
		$length = $_POST['length'];
		$genre_id = $_POST['genre_id'];
		$user_id = $_POST['user_id'];


		$img_file_query = "SELECT img_file FROM songs WHERE id = $id";
		$image_result = mysqli_fetch_assoc(mysqli_query($conn,$img_file_query));
		// var_dump($image_result);
		// die();
		$image = "";
		// check if the user uploaded a new image
		// if not, save the old value of image to $image
		// if yes, do the process of moving files
		if ($_FILES['img_file']['name'] == "") {
			$image = $image_result['img_file'];
		}else{
			$img_destination = "../assets/images/";
			$img_file_name = $_FILES['img_file']['name'];
			move_uploaded_file($_FILES['img_file']['tmp_name'], $img_destination.$img_file_name);
			$image = $img_destination.$img_file_name;
		}


		$song_file_query = "SELECT song_file FROM songs WHERE id = $id";
		$song_result = mysqli_fetch_assoc(mysqli_query($conn,$song_file_query));
		// var_dump($image_result);
		// die();
		$song = "";
		// check if the user uploaded a new image
		// if not, save the old value of image to $image
		// if yes, do the process of moving files
		if ($_FILES['song_file']['name'] == "") {
			$song = $song_result['song_file'];
		}else{
			$song_destination = "../assets/songs/";
			$song_file_name = $_FILES['song_file']['name'];
			move_uploaded_file($_FILES['song_file']['tmp_name'], $song_destination.$song_file_name);		
			$song = $song_destination.$song_file_name;
		}


		$update_song_query = "UPDATE songs SET name = '$name', description = '$description', year = $year, length = $length, genre_id = $genre_id, user_id = $user_id, img_file = '$image', song_file = '$song' WHERE id = $id";
		$result = mysqli_query($conn,$update_song_query);
		header("Location: ../views/catalog.php");
	}else{
		header("Location: " .$_SERVER['HTTP_REFERER']);
	}
 ?>